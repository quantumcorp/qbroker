package org.qbroker.core;

public class Constants {
	public static final class Keys {
		public static final String MQTT_BROKER_PROTOCOL_KEY = "MQTT_PROTOCOL";
		public static final String MQTT_BROKER_IP_KEY = "MQTT_IP";
		public static final String MQTT_BROKER_PORT_KEY = "MQTT_PORT";

		public static final String QBROKER_PORT_KEY = "QBROKER_PORT";

		public static final String SERVICE_NAME_KEY = "SERVICE_NAME";

		public static final String TIMEOUTS_KEY = "TIMOUTS_MS";
	}

	public static final class Defaults {
		public static final String MQTT_BROKER_PROTOCOL = "tcp://";
		public static final String MQTT_BROKER_IP = "localhost";
		public static final String MQTT_BROKER_PORT = "1883";

		public static final String QBROKER_PORT = "4269";

		public static final String SERVICE_NAME = "quantum";
		
		public static final String TIMEOUTS = "1000";
	}
}
