package org.qbroker.core;

import java.io.File;
import java.io.IOException;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.qbroker.core.Constants.Defaults;
import org.qbroker.core.Constants.Keys;
import org.qbroker.mqtt.MQTTAdapter;
import org.qbroker.mqtt.MQTTWrapper;
import org.qbroker.qclient.server.QClientServer;

import de.epileptic.eplib.Registry;
import de.epileptic.eplib.config.GlobalSettings;
import de.epileptic.eplib.log.Log;

public class Main {
	public static void main(String[] args) throws IOException {
		Registry.getInstance().setVerbose(true);

		String configPath = "qbroker.cfg";
		if (args.length > 0)
			configPath = args[0];
		GlobalSettings.setConfigFile(new File(configPath));

		String mqttProtocol = GlobalSettings.get(Keys.MQTT_BROKER_PROTOCOL_KEY, Defaults.MQTT_BROKER_PROTOCOL);
		String mqttIp = GlobalSettings.get(Keys.MQTT_BROKER_IP_KEY, Defaults.MQTT_BROKER_IP);
		String mqttPort = GlobalSettings.get(Keys.MQTT_BROKER_PORT_KEY, Defaults.MQTT_BROKER_PORT);

		int qbrokerPortInt = Integer.parseInt(Defaults.QBROKER_PORT);
		try {
			qbrokerPortInt = Integer.parseInt(GlobalSettings.get(Keys.QBROKER_PORT_KEY, Defaults.QBROKER_PORT));
		} catch (NumberFormatException e) {
			Log.logException(e);
		}

		String serviceName = GlobalSettings.get(Keys.SERVICE_NAME_KEY, Defaults.SERVICE_NAME);

		long timeouts = Integer.parseInt(Defaults.TIMEOUTS);
		try {
			timeouts = Integer.parseInt(GlobalSettings.get(Keys.QBROKER_PORT_KEY, Defaults.QBROKER_PORT));
		} catch (NumberFormatException e) {
			Log.logException(e);
		}

		try {
			MQTTAdapter mqtt = new MQTTAdapter(new MQTTWrapper(mqttProtocol + mqttIp + ":" + mqttPort, timeouts),
					serviceName);
			QClientServer server = new QClientServer(qbrokerPortInt, timeouts, mqtt);
			server.start();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
