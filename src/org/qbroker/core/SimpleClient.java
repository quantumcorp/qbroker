package org.qbroker.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SimpleClient {
	public static void main(String[] args) {
		try {
			Socket s = new Socket(args[0], Integer.parseInt(args[1]));
			BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));

			BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));

			while (s.isConnected() && s.isBound() && !s.isClosed()) {

				// Receives and prints server response
				if (reader.ready())
					System.out.println(reader.readLine());

				// Reads and sends user input
				if (sc.ready()) {
					String line = sc.readLine();

					if (line.equals("X"))
						break;

					// Send
					writer.println(line);
					writer.flush();
				}
			}
			s.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
