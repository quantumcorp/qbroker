package org.qbroker.mqtt;

import java.io.UnsupportedEncodingException;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.qbroker.qclient.server.QClientServer;

/**
 * MQTT adapter for command execution from {@link QClientServer}
 * 
 * @author epileptic
 * @version 1.0
 * @since 0.0
 */
public class MQTTAdapter {
	private MQTTWrapper wrapper;
	private String serviceName;

	public MQTTAdapter(MQTTWrapper wrapper, String serviceName) {
		this.wrapper = wrapper;
		this.serviceName = serviceName;
	}

	/**
	 * Queries information from qNode device via MQTT
	 * 
	 * @param deviceName device at which peripheral for information is connected
	 * @param peripheral from which information will be gathered
	 * @param args       additional arguments for future functionality
	 * @return {@link String} response
	 * @throws MqttException
	 * @throws UnsupportedEncodingException
	 */
	public String get(String deviceName, String peripheral, String args)
			throws MqttException, UnsupportedEncodingException {
		String topicHead = serviceName + "/" + deviceName + "/" + peripheral;
		MQTTMessage msg = new MQTTMessage(args, topicHead + "/get");
		MQTTMessage result = wrapper.query(msg, topicHead + "/state");

		if (result == null)
			return null;
		else
			return result.getPayload();
	}

	/**
	 * Sends command to peripheral
	 * 
	 * @param deviceName parent device of destination peripheral
	 * @param peripheral destination peripheral
	 * @param command    json command
	 * @throws MqttPersistenceException
	 * @throws UnsupportedEncodingException
	 * @throws MqttException
	 */
	public void set(String deviceName, String peripheral, String command)
			throws MqttPersistenceException, UnsupportedEncodingException, MqttException {
		String topic = serviceName + "/" + deviceName + "/" + peripheral + "/command";
		wrapper.publish(new MQTTMessage(command, topic));
	}
}
