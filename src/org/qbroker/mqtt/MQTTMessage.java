package org.qbroker.mqtt;

public class MQTTMessage {
	private String payload;
	private String topic;
	private int qos;
	private boolean retained;

	public MQTTMessage(String payload, String topic, int qos, boolean retained) {
		init(payload, topic, qos, false);
	}

	public MQTTMessage(String payload, String topic) {
		init(payload, topic, 1, false);
	}

	private void init(String payload, String topic, int qos, boolean retained) {
		this.payload = payload;
		this.topic = topic;
		this.qos = qos;
		this.retained = retained;
	}

	public String getPayload() {
		return payload;
	}

	public int getQos() {
		return qos;
	}

	public String getTopic() {
		return topic;
	}

	public boolean isRetained() {
		return retained;
	}
}
