package org.qbroker.mqtt;

import java.io.UnsupportedEncodingException;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.qbroker.mqtt.callbacklistener.CallbackDistributor;

import de.epileptic.eplib.Registry;
import de.epileptic.eplib.log.Log;

/**
 * Simple wrapper for simple replacement of MQTT library if needed.
 * 
 * @author epileptic
 * @version 1.0
 * @since 0.0
 */
public class MQTTWrapper {

	private long queryTimout;

	private MqttClient client;
	private CallbackDistributor dist = new CallbackDistributor();

	/**
	 * @param serverUri   MQTT broker URI
	 * @param queryTimout time in milliseconds before interrupting message listener
	 * @throws MqttException
	 */
	public MQTTWrapper(String serverUri, long queryTimout) throws MqttException {
		this.queryTimout = queryTimout;

		// Initialize connection
		client = new MqttClient(serverUri, MqttClient.generateClientId());
		client.connect();
		client.setCallback(dist);
	}

	/**
	 * @param message {@link MQTTMessage} that will be sent
	 * @throws MqttPersistenceException
	 * @throws UnsupportedEncodingException
	 * @throws MqttException
	 */
	public void publish(MQTTMessage message)
			throws MqttPersistenceException, UnsupportedEncodingException, MqttException {
		client.publish(message.getTopic(), message.getPayload().getBytes(Registry.getInstance().DEFAULT_ENCODING),
				message.getQos(), message.isRetained());
	}

	/**
	 * @param message       query message
	 * @param responseTopic topic where response is expected
	 * @return response as {@link MQTTMessage}
	 * @throws MqttException
	 * @throws UnsupportedEncodingException
	 */
	public MQTTMessage query(MQTTMessage message, String responseTopic)
			throws MqttException, UnsupportedEncodingException {

		// Register listener
		QueryResponseReceiver receiver = new QueryResponseReceiver();
		client.subscribe(responseTopic);
		dist.registerListener(responseTopic, receiver);

		// Publish query
		publish(message);

		MQTTMessage response = null;

		// Wait for result
		long startTime = System.currentTimeMillis();
		while (System.currentTimeMillis() - startTime < queryTimout) {
			// Check if receiver is ready
			if (receiver.isReady()) {
				response = receiver.getResponse();
				break;
			}

			// Sleep because its not working without
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				Log.logException(e);
			}
		}

		// Unregister listener
		dist.unregisterListener(responseTopic, receiver);
		client.unsubscribe(responseTopic);

		return response;
	}
}
