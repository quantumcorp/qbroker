package org.qbroker.mqtt;

import org.qbroker.mqtt.callbacklistener.MqttMessageListener;

import de.epileptic.eplib.log.Log;

/**
 * @author epileptic
 * @version 1.0
 * @since 0.0
 */
public class QueryResponseReceiver implements MqttMessageListener {

	private boolean ready = false;
	private MQTTMessage response = null;

	@Override
	public void onMessage(MQTTMessage message) {
		Log.log(message.getPayload(), this);
		response = message;
		ready = true;
	}

	public boolean isReady() {
		return ready;
	}

	public MQTTMessage getResponse() {
		return response;
	}

}
