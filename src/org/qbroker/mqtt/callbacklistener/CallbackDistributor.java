package org.qbroker.mqtt.callbacklistener;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.qbroker.mqtt.MQTTMessage;

import de.epileptic.eplib.Registry;
import de.epileptic.eplib.log.Log;

/**
 * A listener system wrapped around the {@link MqttCallback} for simpler access.
 * 
 * @author epileptic
 * @version 1.0
 * @since 0.0
 */
public class CallbackDistributor implements MqttCallback {

	private HashMap<String, ArrayList<MqttMessageListener>> listeners = new HashMap<>();

	@Override
	public void connectionLost(Throwable cause) {

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		Log.log("Received " + new String(message.getPayload()) + " on " + topic, this);

		ArrayList<MqttMessageListener> mmls = listeners.get(topic);
		if (mmls != null)
			for (MqttMessageListener i : mmls) {
				i.onMessage(new MQTTMessage(new String(message.getPayload(), Registry.getInstance().DEFAULT_ENCODING),
						topic, message.getQos(), message.isRetained()));
			}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
	}

	public void registerListener(String topic, MqttMessageListener listener) {
		ArrayList<MqttMessageListener> mmls = listeners.get(topic);
		if (mmls == null)
			mmls = new ArrayList<MqttMessageListener>();
		mmls.add(listener);
		listeners.put(topic, mmls);
	}

	public void unregisterListener(String topic) {
		listeners.put(topic, new ArrayList<MqttMessageListener>());
	}

	public void unregisterListener(String topic, MqttMessageListener listener) {
		ArrayList<MqttMessageListener> mmls = listeners.get(topic);
		mmls.remove(listener);
	}

}
