package org.qbroker.mqtt.callbacklistener;

import org.qbroker.mqtt.MQTTMessage;

public interface MqttMessageListener {
	public void onMessage(MQTTMessage message);
}
