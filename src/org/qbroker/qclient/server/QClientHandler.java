package org.qbroker.qclient.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.qbroker.mqtt.MQTTAdapter;
import org.qbroker.qclient.server.types.QClientCommandType;
import org.qbroker.qclient.server.types.QClientResponseType;

import de.epileptic.eplib.log.Log;

/**
 * Handles a connection accepted by {@link QClientServer}. Executes command sent
 * by this client by forwarding it to the right MQTT device.
 * 
 * @author epileptic
 * @version 1.0
 * @since 0.0
 *
 */
public class QClientHandler implements Runnable {
	private long commandTimeout;

	private Socket client;
	private BufferedReader reader;
	private PrintWriter writer;

	private MQTTAdapter mqtt;

	/**
	 * Constructs a {@link QClientHandler} handling the passed {@link Socket}t as a
	 * QClient connection.
	 * 
	 * @param client         {@link Socket} that has to be handled
	 * @param commandTimeout time in milliseconds after which the connection has to
	 *                       be closed.
	 * @throws IOException if an exception is been thrown during reader and printer
	 *                     instantiation
	 */
	public QClientHandler(Socket client, long commandTimeout, MQTTAdapter mqtt) throws IOException {
		this.client = client;
		this.commandTimeout = commandTimeout;
		this.mqtt = mqtt;

		reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
		writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));

		Log.log("Initialized for " + client.getInetAddress().toString(), this);
	}

	@Override
	public void run() {
		Log.log("Handling " + client.getInetAddress().toString(), this);

		// Save start time for timeout determination
		long startTime = System.currentTimeMillis();

		String line = null;
		// Checks for timeout
		while (System.currentTimeMillis() - startTime < commandTimeout) {
			try {
				// Waits for reader to get ready
				if (reader.ready()) {
					line = reader.readLine();
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (line != null) {
			Log.log("Received " + line, this);
			String[] value = line.split(";");

			// Response reservation and initialization
			QClientResponseType responseCode = QClientResponseType.ERROR;
			String responseData = null;

			// Checks for right package size
			if (value.length > 3) {
				// Wrap payload
				QClientCommandType command = QClientCommandType.valueOf(value[0]);
				String address = value[1];
				String peripheral = value[2];
				String payload = value[3];

				switch (command) {
				case SET:
					try {
						mqtt.set(address, peripheral, payload);
						responseCode = QClientResponseType.OK;
					} catch (UnsupportedEncodingException | MqttException e) {
						responseCode = QClientResponseType.ERROR;
						e.printStackTrace();
					}
					break;

				case GET:
					try {
						responseData = mqtt.get(address, peripheral, payload);
						if (responseData != null)
							responseCode = QClientResponseType.OK_DATA;
						else
							responseCode = QClientResponseType.ERROR;
					} catch (MqttException | UnsupportedEncodingException e) {
						responseCode = QClientResponseType.ERROR;
						e.printStackTrace();
					}
					break;
				}

				// Sanitize output
				if (responseCode == QClientResponseType.OK_DATA) {
					if (responseData == null) {
						responseCode = QClientResponseType.OK;
					}
				} else {
					responseData = null;
				}
			}

			// Send response
			writer.println(responseCode.toString());
			if (responseData != null)
				writer.println(responseData);
			writer.flush();

		} else {
			Log.logWarn("No message received", this);
		}

		try {
			// Close client
			client.close();

			Log.log("Closed", this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
