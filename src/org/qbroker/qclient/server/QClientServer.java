package org.qbroker.qclient.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.qbroker.mqtt.MQTTAdapter;

import de.epileptic.eplib.log.Log;

/**
 * A server which accepts clients on a {@link ServerSocket} and starts a
 * {@link QClientHandler} for each.
 * 
 * @author epileptic
 * @version 1.0
 * @since QBroker 0.0
 */
public class QClientServer extends Thread {

	private ServerSocket server;

	private ExecutorService pool;

	private long commandTimeout;

	private MQTTAdapter mqtt;

	/**
	 * Opens a {@link ServerSocket} and accepts all clients connecting to it. Every
	 * client will enqueue a {@link QClientHandler} on the thread pool. Amount of
	 * parallel running threads is defined by parameter <code>threads</code>
	 * 
	 * @param port           for {@link ServerSocket}
	 * @param threads        parallel running {@link QClientHandler}s
	 * @param commandTimeout timeout before connection to client will be closed
	 * @throws IOException if {@link ServerSocket} instantiation fails
	 */
	public QClientServer(int port, int threads, long commandTimeout, MQTTAdapter mqtt) throws IOException {
		init(port, threads, commandTimeout, mqtt);
	}

	/**
	 * Opens a {@link ServerSocket} and accepts all clients connecting to it. Every
	 * client will enqueue a {@link QClientHandler} on the thread pool. Amount of
	 * parallel running threads is defined by logical processor cores available.
	 * Value determined by <code>{@link Runtime}.availableProcessors()</code>
	 * 
	 * @param port           for {@link ServerSocket}
	 * @param commandTimeout timeout before connection to client will be closed
	 * @throws IOException if {@link ServerSocket} instantiation fails
	 */
	public QClientServer(int port, long commandTimeout, MQTTAdapter mqtt) throws IOException {
		init(port, Runtime.getRuntime().availableProcessors(), commandTimeout, mqtt);
	}

	/**
	 * @param port    to that the server will be bound
	 * @param threads max count of parallel running ClientHandler
	 * @throws IOException if ServerSocket instantiation fails
	 */
	private void init(int port, int threads, long commandTimeout, MQTTAdapter mqtt) throws IOException {
		this.commandTimeout = commandTimeout;
		this.mqtt = mqtt;

		server = new ServerSocket(port);
		pool = Executors.newFixedThreadPool(threads);
	}

	@Override
	public void run() {
		while (server.isBound() && !server.isClosed() && !this.isInterrupted()) {
			try {
				// Accept connection
				Socket client = server.accept();
				Log.log("Client connected", this);

				// Create new ClientHandler and pass connected client
				pool.execute(new QClientHandler(client, commandTimeout, mqtt));
				Log.log("Enqueued to thread pool", this);

				// Run garbage collector
				System.gc();
				Log.log("Started garbage collector", this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
