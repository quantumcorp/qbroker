package org.qbroker.qclient.server.types;

public enum QClientCommandType {
	SET, GET
}
